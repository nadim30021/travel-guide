<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
	<script src="{url}public/scripts/jquery-3.1.1.js" type="text/javascript"></script>
	<script src="{url}public/scripts/registration_validation.js" type="text/javascript"></script>
</head>

<center>
<body onload="des()">
	<h2>Registration</h2>
			<form method="post" onsubmit="return validate();" id="myform">
				<table cellpadding="4" cellspacing="4">
					<tr>
						<td>FIRST NAME</td>
						<td><input type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name')?>" onkeyup="des()"/>	
						</td>
					</tr>
					<tr>
						<td>LAST NAME</td>
						<td><input type="text" name="last_name" id="last_name" value="<?php echo set_value('last_name')?>" onkeyup="des()"/></td>
					</tr>
					<tr>
						<td>DOB</td>
						<td>
						<Select name = "Day" id="Day">
						<option value = "none" >DD</option>
						<?php
							for($i=1;$i<32 ; $i++)
							{
						?>
							<option value = "<?php if(set_value('Day')) echo set_value('Day'); else echo'$i'; ?>" > <?php echo"$i"; ?></option>
						<?php	
							}
						?>
						</Select>
						<Select name = "Month" id="Month" >
						<option value = "none">MM</option>
						<?php
							for($i=1;$i<13 ; $i++)
							{
						?>
							<option value = <?php if(set_value('Month')) echo set_value('Month'); else echo"$i"?>> <?php echo"$i"?></option>
						<?php	
							}
						?>	
						</Select>
						 <Select name = "Year" id="Year" >
						<option value = "none">YYYY</option>
						<?php
							for($i=1980;$i<2016 ; $i++)
							{
						?>
							<option value = <?php if(set_value('Year')) echo set_value('Year'); else echo"$i"?>> <?php echo"$i"?></option>
						<?php	
							}
						?>					
						</Select>
						</td>
					</tr>
					<tr>
					<td>GENDER</td>
					<td><input type="radio" name='gender' id="male" value="male">Male <input type="radio" name='gender' id="id" value='female'>Female<input type="radio" name='gender' id="other" value='other'>Other</td>
					</tr>
					<tr>
						<td>EMAIL</td>
						<td><input type="text" name="email" id="email" value="<?php echo set_value('email')?>" onchange="loadEmail()" onkeyup="des()" /></td><td>
					</tr>
					<tr>
						<td>PHONE</td>
						<td><input type="text" name="phone" id="phone" value="<?php echo set_value('phone')?>" onkeyup="des()"/></td>
					</tr>
					<tr>
						<td>PASSWORD</td>
						<td><input type="password" id="password" name="pass" onkeyup="des()" onchange="passlengthchk()"/> </td><td><span id="passsidemsg"></span></td>
					</tr>
					<tr>
						<td>CONFIRM PASSWORD</td>
						<td><input type="password" name="conf_pass" id="conf_pass" onkeyup="des()" onchange="confrpass()" /></td><td><span id="sidemsg2"></span></td>
					</tr>
					
					
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="buttonRegistration" value="Register" id="buttonRegistration" /></td>
					</tr>
				</table>
			</form>

			<?php echo $message; ?>


			<span id="span_first_name" style="color:red"></span>
			<span id="span_last_name" style="color:red"></span>
			<span id="span_email" style="color:red"></span>
			<span id="span_phone" style="color:red"></span>
			<span id="span_password"></span>
			<span id="span_conf_pass"></span>
			<span id="span_gender" ></span>

</body>
</center>
</html>