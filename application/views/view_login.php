<!DOCTYPE html>
<html>
	<head>
		<title>Login</title>
		<style type="text/css">
			body{
				font-family: Arial;
			}
		</style>
		<script src="{url}public/scripts/jquery-3.1.1.js" type="text/javascript"></script>
		<script src="{url}public/scripts/login_validation.js" type="text/javascript"></script>
	</head>
	<body>
		<center>
			<h2>LOGIN</h2>
			<form method="post" onsubmit="return validate();">
				<table cellpadding="4" cellspacing="4">
					<tr>
						<td>EMAIL</td>
						<td><input type="text" name="email" id="email" value="<?php echo set_value('email')?>" /></td>
					</tr>
					<tr>
						<td>PASSWORD</td>
						<td><input type="password" name="password" id="password" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="buttonLogin" value="Login" /></td>
					</tr>
				</table>
			</form>
			<br />
			
			<label>{message}</label>

		</center>
		<span id="span_email"></span>
		<span id="span_password"></span>
	</body>
</html>