<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Showallmodel extends CI_Model {



	public function getPlacesImage()
	{
		$sql = "SELECT * FROM accounts";

		//$result = mysqli_query($sql);
		$result = $this->db->query($sql);

		//$row = mysqli_fetch_array($result);
		return $result->result_array();
	}





	public function getHomePagesImage()
	{
		$this->db->select('locationID');
		$this->db->select('image1');
		$this->db->select('image2');
		$this->db->select('name');
		$this->db->select('description');
		$result = $this->db->get('places');
		$data=$result->result_array();
		
		
		
		
		return $data;
	}

	
	public function getimagedetails($locid)
	{
		$this->db->select('imgURL');
		$this->db->where('locationID',$locid);
		$result = $this->db->get('places_image');
		$data=$result->result_array();

		return $data;
	}
	
	
	
	public function getlocationdescription($locid)
	{
		$this->db->select('description');
		$this->db->where('locationID',$locid);
		$result = $this->db->get('places');
		$data=$result->result_array();

		return $data;
	}


	public function getPlaceRating()
	{
		$this->db->select('name');
		$this->db->select('location');
		$this->db->select('vote');
		$this->db->order_by("vote", "desc");
		$result = $this->db->get('places');
		$data=$result->result_array();

		return $data;
	}
	
	
	
	
	public function getUserVote()
	{
		$this->db->select('user_vote.user_email');
		$this->db->select('places.name');
		$this->db->select('places.location');
		$this->db->join('user_vote','places.locationID = user_vote.vote');
		
		$this->db->order_by("user_vote.user_email", "asc");
		$result = $this->db->get('places');
		$data=$result->result_array();

		return $data;
	}

	
}