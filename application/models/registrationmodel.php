<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrationmodel extends CI_Model {

	public function addUser($fname, $lname, $day, $month, $year, $gender, $email, $phone, $pass)
	{
		$dob=$year.'-'.$month.'-'.$day;
		$user_data = array(
			'firstName'=>$fname,
			'lastName'=>$lname,
			'dob'=>$dob,
			'gender'=>$gender,
			'email'=>$email,
			'phone'=>$phone,
			'password'=>$pass,
			'privilige'=>'111',
			'resetPass'=>'0'
		);
		$this->db->insert('user_info',$user_data);
		return true;
	}
	
	
public function getUserEmail($email)
{
	$this->db->where('email', $email);
	$result = $this->db->get('user_info');
	$result = $result->row_array();

	if($result)
	{
		return "User already exist";
	}
	else
	{
		return "";
	}
}
	
	
	
}