<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {




	public function __construct()
	{
		parent::__construct();
		//$this->load->library('session');
		if(! $this->session->userdata('loggedin'))
		{
			$this->load->helper('url');
			redirect(base_url().'login', 'refresh');
			return;
		}

		$this->load->model('loginmodel');
	}

	public function index()
	{
		$this->load->model('showallmodel');
		$this->load->library('parser');
		
			$data['url']=base_url() ;
			
			$data['homepageimg'] = '';
			//print_r($data);

			$this->parser->parse('view_admin',$data);
			
		
	}

	public function userdetails($account_no = 0)
	{
		
		
		$user = $this->loginmodel->findUser($account_no);
		if($user == null)
		{
			echo "user not found";
		}
		else
		{
			$data['url']=base_url() ;
			$this->load->view('view_details', $user);
		}
	}
	
	
	
	public function seeUserVote()
	{
		
		$this->load->model('showallmodel');
		$this->load->library('parser');
		
		
			$data['usermail']=$this->session->userdata('usermail');
			$data['userVoteList'] = $this->showallmodel->getUserVote();
			$data['url']=base_url() ;

		$this->parser->parse('view_userVote',$data);
	}
	
	
	public function logout()
	{
			$this->session->sess_destroy();
			$data['url']=base_url() ;
			$data['message']='';
			$this->load->view('view_login',$data);
	}
	
}
