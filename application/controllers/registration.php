<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller {

	public $pass = "";

	public function index()
	{
			
		if($this->input->post('buttonRegistration'))
		{
			if ($this->form_validation->run('registration') == FALSE)////IF FORM DOES NOT VALIDATED
            {
            	$data['message'] = validation_errors();
                $this->load->view('view_registration',$data);
            }
            else////FORM DATA IS OKAY
            {
                $fname = $this->input->post('first_name');
				$lname = $this->input->post('last_name');
				$day = $this->input->post('Day');
				$month = $this->input->post('Month');
				$year = $this->input->post('Year');
				$gender = $this->input->post('gender');
				$email= $this->input->post('email');
				$phone= $this->input->post('phone');
				$pass= md5($this->input->post('pass'));

				$this->load->model('registrationmodel');

				if($this->registrationmodel->adduser($fname, $lname, $day, $month, $year, $gender, $email, $phone, $pass))
				{
					redirect(base_url().'login', 'refresh');
				}
            }
		}
		else
		{
			$data['message']='';
			$data['url']=base_url() ;
			$this->load->view('view_registration',$data);
		}
	}

	public function checkDay($day)
	{
		if($day != "none")
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('checkDay', 'Please select day');
			return false;
		}
	}
	public function checkMonth($month)
	{
		if($month != "none")
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('checkMonth', 'Please select Month');
			return false;
		}
	}
	public function checkYear($year)
	{
		if($year != "none")
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('checkYear', 'Please select Year');
			return false;
		}
	}
	public function checkPhone($phone)
	{
		if(strlen($phone) == 11)
		{
			if(!is_numeric($phone)) {
			    $this->form_validation->set_message('checkPhone', 'Invalid Phone number. Phone number must contain only digits.');
			    return false;
			}
			return true;
		}

		else
		{
			$this->form_validation->set_message('checkPhone', 'Phone number must contain exactly 11 digits');
			return false;
		}
	}
	public function checkPassword($password="")
	{
		if (strlen($password) > 7)
		{
			$x = preg_match('^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$^',$password);
			if($x)
			{
				$this->pass = $password;
				return true;
			}
			else
			{
				$this->form_validation->set_message('checkPassword', 'Invalid password. Must contain one uppercase,one lowercase letter,one number');
				return false;
			}
		}
		else
		{
			$this->form_validation->set_message('checkPassword', 'Invalid password. Password Must be Minimum 8 charecters');
			return false;
		}
		return $password;
		
	}
	public function confirmPassword($password)
	{
		if ($password == $this->pass)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('confirmPassword', 'Password and Confirm Password do not match');
			return false;
		}
		
	}
	
	
	
	public function verifyEmail()
	{
	$email = $this->input->get_post('mail');
	$this->load->model('registrationmodel');	
	$result = $this->registrationmodel->getUserEmail($email);
	echo $result;
	}
	
	
}
