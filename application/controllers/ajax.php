<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function addlike()
	{
		
		$usermail= $this->session->userdata('usermail');
		
		$id = $this->input->get_post('locid');
		
		$this->db->select('vote');
		$this->db->where('locationID', $id);
		$res=$this->db->get('places');
		$row = $res->row_array();

			
		$upar= array(
               'vote' => $row['vote']+1
            );
		$this->db->where('locationID', $id);
		$this->db->update('places', $upar);
		
		$emvo=array(
				'user_email'=>$usermail,
				'vote'=>$id
				);
		
		$this->db->insert('user_vote',$emvo);
		
	}
	
	public function removelike()
	{
		
		$usermail= $this->session->userdata('usermail');
		$id = $this->input->get_post('locid');
		
		$this->db->select('vote');
		$this->db->where('locationID', $id);
		$res=$this->db->get('places');
		$row = $res->row_array();
		$upar= array(
               'vote' => $row['vote']-1
            );
		$this->db->where('locationID', $id);
		$this->db->update('places', $upar); 
		
		
		$this->db->where('user_email',$usermail);
		$this->db->where('vote',$id);
		$this->db->delete('user_vote');

		
	}
	
	
	
	
	public function getUserLike()
	{
		
		$usermail= $this->input->get_post('usermail');
		
		
		$this->db->select('vote');
		$this->db->where('user_email', $usermail);
		$res=$this->db->get('user_vote');
		$votelist = $res->result_array();
		$outputstr = '';
		
		foreach($votelist as $vote)
		{
			$outputstr .= "$vote[vote]";
		}
		
		
		echo $outputstr;
		
	}
	
	
	
}