<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {




	public function __construct()
	{
		parent::__construct();
		//$this->load->library('session');
		if(! $this->session->userdata('loggedin'))
		{
			$this->load->helper('url');
			redirect(base_url().'login', 'refresh');
			return;
		}

		$this->load->model('loginmodel');
	}

	public function index()
	{
		$this->load->model('showallmodel');
		$this->load->library('parser');
		
			$data['url']=base_url() ;
			$data['usermail']=$this->session->userdata('usermail');
			$data['homepageimg'] = $this->showallmodel->getHomePagesImage();
		
			$this->parser->parse('view_home',$data);
			
		
	}

	public function imagedetails($locid=0)
	{
		$this->load->model('showallmodel');
		$this->load->library('parser');
		
		$data['imgdtls'] = $this->showallmodel->getimagedetails($locid);
		
		$data['locdescip'] = $this->showallmodel->getlocationdescription($locid);
		$data['url']=base_url() ;
		$this->parser->parse('view_imgdetails',$data);
	}
	
	
	
	
	
	public function newpicupload()
	{
		
		$this->load->library('parser');
		
		$data['usermail']=$this->session->userdata('usermail');
		
		$data['url']=base_url() ;
		
		$this->parser->parse('view_newpicupload',$data);
	}
	
	
	public function showMyPic()
	{
		
		$this->load->library('parser');
		
		$data['usermail']=$this->session->userdata('usermail');
		
		$data['url']=base_url() ;
		
		$this->parser->parse('view_showMyPic',$data);
	}
	
	
	
	public function removePic()
	{
		
		$this->load->library('parser');
		
		$data['usermail']=$this->session->userdata('usermail');
		$data['url']=base_url() ;
		$this->parser->parse('view_removePic',$data);
	}
	
	
	public function suggestAdmin()
	{
		
		$this->load->library('parser');
		
		$data['usermail']=$this->session->userdata('usermail');
		
		$data['url']=base_url() ;
		
		$this->parser->parse('view_suggestAdmin',$data);
	}
	
	
	
	public function seeRating()
	{
		
		$this->load->model('showallmodel');
		$this->load->library('parser');
		
		
			$data['usermail']=$this->session->userdata('usermail');
			$data['placeRating'] = $this->showallmodel->getPlaceRating();
		
		$data['url']=base_url() ;
		$this->parser->parse('view_seeRating',$data);
	}
	
	
	
	
	public function logout()
	{
			$this->session->sess_destroy();
			
			$data['message']='';
			
			redirect(base_url().'login', 'refresh');
	}
	
}
