<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Start extends CI_Controller {

	public function index()
	{

		$this->load->library('parser');
		
		if($this->session->userdata('loggedin'))
		{
				redirect(base_url().'home', 'refresh');
		}
		
		else
		{
					
			$data['message']='';
			$data['url']=base_url();

			//$this->session->sess_destroy();
			$this->parser->parse('index',$data);		
		}
	}
	
}