<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		
		if($this->session->userdata('loggedin'))
		{
			redirect(base_url().'home', 'refresh');
		}
		else{

			if($this->input->post('buttonLogin'))
			{
				if ($this->form_validation->run('login') == FALSE)
	            {
	            	$data['message'] = validation_errors();
	                $this->parser->parse('view_login',$data);
	            }
				
				else
				{
					$email = $this->input->post('email');
					$ps = md5($this->input->post('password'));

					$this->load->model('loginmodel');

					if($this->loginmodel->verifyUser($email, $ps)=='user')
					{
								
						$this->session->set_userdata('usermail', $email);
						
						
						
						$this->session->set_userdata('loggedin', true);
						redirect(base_url().'home', 'refresh');			
					}

					else if($this->loginmodel->verifyUser($email, $ps)=='admin')
					{			
						$this->session->set_userdata('loggedin', true);
						redirect(base_url().'admin', 'refresh');
					}

					else
					{
						$data['message']='Invalid Username or Password';
						$data['url']=base_url() ;
						$this->parser->parse('view_login',$data);
					}
				}
			}
			else
			{	
				$data['message']='';
				$data['url']=base_url() ;
				$this->parser->parse('view_login',$data);		
			}
		}
	}
}