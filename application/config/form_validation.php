<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config = array(
                 'registration' => array(
                                    array(
                                            'field' => 'first_name', //HTML FIELD NAME
                                            'label' => 'First Name', //Error Msg 
                                            'rules' => 'required' //RULE
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'required'
                                         ),
									array(
                                            'field' => 'Day',
                                            'label' => 'Day',
                                            'rules' => 'callback_checkDay'
                                         ),
                                    array(
                                            'field' => 'Month',
                                            'label' => 'Month',
                                            'rules' => 'callback_checkMonth'
                                         ),
                                    array(
                                            'field' => 'Year',
                                            'label' => 'Year',
                                            'rules' => 'callback_checkYear'
                                         ),
                                    array(
                                            'field' => 'gender',
                                            'label' => 'Gender',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'phone',
                                            'label' => 'Phone number',
                                            'rules' => 'required|callback_checkPhone'
                                         ),
                                    array(
                                            'field' => 'pass',
                                            'label' => 'Password',
                                            'rules' => 'required|callback_checkPassword'
                                         ),
                                    array(
                                            'field' => 'conf_pass',
                                            'label' => 'Confirm password',
                                            'rules' => 'required|callback_confirmPassword'
                                         )
                                    ),
                 'login' => array(
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required'
                                         )                         
               )
        );