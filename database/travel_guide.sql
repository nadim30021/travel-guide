-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2017 at 06:02 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `travel_guide`
--

-- --------------------------------------------------------

--
-- Table structure for table `image_keyword`
--

CREATE TABLE IF NOT EXISTS `image_keyword` (
  `imageId` int(5) NOT NULL,
  `keywID` int(5) NOT NULL,
  KEY `imageId` (`imageId`),
  KEY `keywID` (`keywID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `keyword`
--

CREATE TABLE IF NOT EXISTS `keyword` (
  `keywID` int(5) NOT NULL,
  `keyword` varchar(20) NOT NULL,
  PRIMARY KEY (`keywID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE IF NOT EXISTS `places` (
  `locationID` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `location` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `image1` varchar(50) NOT NULL,
  `image2` varchar(50) NOT NULL,
  `vote` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`locationID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`locationID`, `name`, `location`, `description`, `latitude`, `longitude`, `image1`, `image2`, `vote`) VALUES
(1, 'Cox''s Bazar', 'Cox''s Bazar,Chittagong', 'Cox''s Bazar is a town, a fishing port and district headquarters in Bangladesh. The beach in Cox''s Bazar is an unbroken 120  km sandy sea beach with a gentle slope, one of the world''s longest. It is located 150 km south of the industrial port Chittagong. Cox''s Bazar is also known by the name Panowa, whose literal translation means "yellow flower." Its other old name was "Palongkee".The modern Cox''s Bazar derives its name from Captain Hiram Cox (died 1799), an officer of the British East India Com', 21.45, 91.97, 'coxbazar1.jpg', 'coxbazar2.jpg', 1),
(2, 'Himchori', 'Marine Dr Himchori,Cox''s Bazar,Bangladesh', 'Many attractions are for the tourists around Cox''s Bazar. You can go Himchori to see it''s Waterfall, this is famous for waterfall, though in the winter season it dwindles but during the rainy season it''s really a fabulous and full waterfall can be enjoyed.\r\n1. Beautiful Waterfall: \r\nThe amazing waterfall of Himchari is a very rare scene to enjoy. The waterfall from the green hill is relatively extraordinary to look at. However, in the winter it dwindles whereas in the rainy season it is really wonderful and full waterfall could be enjoyed. The spot is ideal for picnic, shooting, relaxing and sunbathing. But whenever you have sunbathing locals can rush around you as Bangladeshis'' are not used to see women in swim costume. Here you get beautiful hilltop resort centre where you can stay for relaxing and can hear the shore of the Bay of Bengal. \r\n2. Himchari National Park: \r\nPark is a beautiful tropical rain forest (evergreen & semi-ever-green) around the South Asia. The park was establish', 21.344536, 92.031902, 'himchori1.jpg', 'himchori2.jpg', 1),
(3, 'Bandarban', 'Bandarban,Chittagong', 'Bandarban is a district in South-Eastern Bangladesh, and a part of the Chittagong Division. It is one of the three districts that make up the Chittagong Hill Tracts, the others being Rangamati District and Khagrachhari District. Bandarban is regarded as one of the most attractive travel destinations in Bangladesh. Bandarban (meaning the dam of monkeys), or in Marma or Arakanese language as "Rwa-daw Mro" is also known as Arvumi or the Bohmong Circle (of the rest of the three hill districts Rangamati is the Chakma Circle, Raja Devasish Roy and Khagrachari is the Mong Circle, Raja Sachingprue Marma). Bandarban town is the home town of the Bohmong Chief (currently King, or Raja, U Cho Prue Marma) who is the head of the Marma population. It also is the administrative headquarters of Bandarban district, which has turned into one of the most exotic tourist attractions in Bangladesh.', 21.8311, 92.368632, 'bandarban1.jpg', 'bandarban2.jpg', 1),
(4, 'Saint Martin''s Island', 'Saint Martin,Cox''s Bazar,Chittagong\r\n', 'St. Martin''s Island is a small island (area only 8 km2) in the northeastern part of the Bay of Bengal, about 9 km south of the tip of the Cox''s Bazar-Teknaf peninsula, and forming the southernmost part of Bangladesh. There is a small adjoining island that is separated at high tide, called Chera Dwip. It is about 8 kilometres (5 miles) west of the northwest coast of Myanmar, at the mouth of the Naf River.', 20.923168, 92.267641, 'saintmartin1.jpg', 'saintmartin2.jpg', 2),
(5, 'Ahsan Manzil', 'Kumartuli,Islampur,Dhaka\r\n', 'Ahsan Manzil is an attractive tourist spot in Dhaka. It is one of the most meaningful architectural heritage of Bangladesh. It is situated in Old Dhaka on the bank of the river Buriganga. The area is called Kumartuli in Islampur. Here you you’ll enjoy the feeling of the lifestyle of the Nawabs of Dhaka. The pink palace was actually built by Nawab Sir Abdul Gani in 1872, and was reconstructed after the tornado of 1888. Now it has been established as a museum. People have the opportunity to visit the museum and here you will find some historical and archeological things.It has 23 galleries displaying portraits, furniture and other objects used by the Nawab.', 23.708596, 90.406002, '', '', 0),
(6, 'Lalbagh Fort', 'Lalbagh,Dhaka', 'Lalbagh fort is a Mughal palace. It is one of the greatest heritage site of Dhaka, Bangladesh. It was built in 1678 A.D. by Muhammad Azam Shah and continued the work by his successor Nawab Shaista Khan. It has a huge area and many hidden passages. The fort has three important ancient buildings, Pari Bibir Mazar, Darbar Hall and a Mosque. Memorial of Pari Bibi was designed by marbles, two fount and different types of flora. Three domed Mosque shows the beauty of Mughal Architecture.Now there is a museum for the visitors to have the opportunity to see the ancient things used by the Nawab Shaista khan, paintings, furniture as well as weapons used in wars at Mughal period. Everyday many people come here to enjoy this beautiful and historical fort.', 23.718176, 90.386607, '', '', 1),
(7, 'Dhakeshwari National Temple', 'Dhakeshwari Kali Temple, Amimpur, Old Dhaka(Beside BUET), Bangladesh', 'Dhakeshwari National Temple is a famous Hindu temple in Dhaka. It is the national Temple of Bangladesh. The name “Dhakeshwari” means “Goddess of Dhaka”. This Temple has assumed status as the most important Hindu place of worship in Bangladesh. It was built in the 12th century by Ballal Sen, but its architecture has been changed because of numerous repairs, renovations and rebuilding in its long years of existence. But still it is an attractive heritage place. This temple is a hub of socio-cultural as well as religious activity. Each year, the largest celebration of Durga Puja in Dhaka is held at this National Temple. Janmashthami procession event also start from here in each year.', 23.72449, 90.388906, '', '', 0),
(8, 'National Martyr''s Memorial', 'Nabinagar, Savar, Dhaka\r\n', 'National Martyrs’ Memorial is a monument in Bangladesh. In Bangladesh it is known as “Jatiyo Sriti Shoudho”. It is the symbol of the valour and the sacrifice of those killed in the Bangladesh libaration war of 1971. It is built with Concrete, but made of blood. It stands 150 feet tall, but every martyr it stands for stands so much taller. It is an achievement the dimensions of which can be measured but it stands for an achievement which is immeasurable. The monument is located in Savar, about 35km north-west from Dhaka.', 23.911216, 90.25472, '', '', 0),
(9, 'National Parliament House', 'Sher-e- Bangla Nagar, Dhaka', 'The National Parliament House or ‘Jatiyo Sangshad Bhaban’ is located in capital Dhaka with area of 208 acres. It was designed by Louis Kahn and made of concrete and marble showing a rich blend of ancient and modern architecture. Use of different geometrical shape and floating outlook are some different attractions of this glorious creation. You can visit only surroundings and lakes of the Parliament as it is not open for all.\r\n', 23.76245, 90.378661, '', '', 0),
(10, 'National Museum', 'Shahbagh, Dhaka\r\n', 'National Museum is the biggest museum in Bangladesh and one of the largest museums in South Asia. It has several departments like archaeology, classical, decorative and contemporary art, history, natural history, ethnography and world civilization of displays. It has a rich collection of paintings, old coins, metal images, world famous embroidered quilts (Nakshi kantha) and much more. You can also find valuable articles of the heroic liberation war here. The Museum is noted for its collection of Shilpacharya Zainul Abedin and works of other contemporary artists. The massive four storey building has not only a large exhibition halls, but also conservatory laboratory, library, three auditoriums, photographic gallery, temporary exhibition hall and an audio-visual division.\r\n', 23.737512, 90.394573, '', '', 0),
(11, 'Sundarban', 'Sundarban, Khulna\r\n', 'Sundarban is the world’s largest mangrove forest. It is famous for the Royal Bengal Tiger, there are about 400 Royal Bengal Tiger in this forest. You can also find beautiful spotted deer in this forest as well as crocodiles, varieties of birds , monkey and many other wild animals. Sundari tree a type of mangrove that is extensively found here. The main place of Sundarban is Harbaria, Katka, Kachikhali, Hiron point. The main attraction of Sundarbans are wildlife photography including photography of the famous Royal Bengal Tiger. Here you will enjoy wildlife viewing, nature study, meeting fishermen, wood-cutters and honey-collectors as well as peace and tranquility in the wilderness. Every year thousands of locals and foreigners come to Bangladesh to visit this unique mangrove forest and they enjoy its dazzling beauty very much.\r\n', 21.949722, 89.183333, '', '', 0),
(12, 'Shat Gambuj Masjid', 'Bagerhat,Khulna\r\n', 'This is one of the most beautiful and largest archaeological and historical Mosque in Bangladesh. This is a Holy place. It was made by red burn mud.it was decorated beautifully with terracotta flowers and foliage. The archeological beauty of this Mosque enchants the tourist very much. Besides this Mosque an attractive archaeological museum is there where you can find that times archaeological and historical materials. Near to this mosque you can also visit the Shrine of Khan Jahan Ali. The UNESCO has been recognized this Mosque as a World Heritage Site in 1983. At present a picnic spot has been established near this heritage site at the historical Ghora Dighi.\r\n', 22.674596, 89.741957, '', '', 0),
(13, 'Baitul Mukarram Mosque', 'Bangabandhu National Stadium Road,Paltan,Dhaka\r\n', 'Baitul Mukarram Mosque is the National Mosque of Bangladesh. It is designed in the style of the holy Ka’aba of Mecca. Its construction started in 1960 and was completed in 1968. In this eight stories mosque about 40,000 people can say their prayer at a time. It is the biggest mosque in Dhaka city and 10th biggest mosque in the world. This uniquly designed mosque has severel modern architectural features as well as it preserves the traditional principles of Mughal architecture. The mosque complex includes shops, libraries, offices and parking areas. It has a very beatiful Moghul styled garden that attracts people very much. Non-Muslims can normally enter outside of prayer time.\r\n', 23.729476, 90.412243, '', '', 0),
(14, 'Boga Lake', 'Boga Mukh Para, Ramu, Bandarban\r\n', 'Boga Lake is the highest natural lake of Bangladesh situated at Bandarban. Boga Lake is around 10 acres in size and situated 1800 feet above the sea level, surrounded by hills. Lake water is so crystal clear that one can see fishes under the beautiful blue colored transparent water. Another wonderful point about this lake is, it changes its colour from time to time.You can enjoy camp fire beside the lake, that will be unbelievable and mind-blowing memory in your life. There is a small Tribal ‘Bawm’ village just beside of the lake. One night in this ‘Bawm’ village can be a great experience.\r\n', 21.980335, 92.469996, '', '', 0),
(15, 'Paharpur', 'Paharpur Bihar Museum, Naogaon\r\n', 'Paharpur is a Buddhist temple was found under ground. The name of the Buddhist Temple is Sompur Bihar . Somapura Mahavihara in Paharpur, Badalgachhi Upazila, Naogaon District, Bangladesh (25°1’51.83?N, 88°58’37.15?E) is among the best known Buddhist viharas in the Indian Subcontinent and is one of the most important archeological sites in the country. It was designated a UNESCO World Heritage Site in 1985\r\n', 25.030864, 88.976986, '', '', 0),
(16, 'Nilgiri', 'Nilgiri, Bandarban\r\n', 'Do anyone walk over the cloud? then must go to Nilgiri. One of the highest peaks in Bangladesh.A spectacular heavenly place in Bandarban, managed by Army. Here tourist can get varieties of seasonal beauty. Can get cloudy experiances, campfire activities and see foggy gesture of nature. From here, nice and clear view over the tortuous Sangu River as far as visitors sight go.\r\n', 21.911868, 92.325398, '', '', 0),
(17, 'Nilachal', 'Nilachal, Bandarban\r\n', 'To enjoy the zigzag hilly roads, tourists must visit the Nilachal, which is the nearest from the main town. From here visitors can view the whole Bandarban at a glance. It is near to Meghla and It’s about 2000 feet above the sea level. This place is also called the “Tiger Hill”. Clouds are catchable here in every early morning and at night.\r\n', 22.166923, 92.212153, '', '', 0),
(18, 'Kantajew Temple', 'Kaharol Upzilla, Sondurpur, Dinajpur\r\n', 'Kantajew Temple is the most ornate and beautiful temple of Bangladesh. It is treated as one of the finest masterpieces of medieval architecture of Bengal made by red burn mud. Every inch of the temple surface is embellished with exquisite ‘Terracotta’ plaques, representing flora fauna, geometric motifs, mythological scenes and an astonishing array of contemporary social scenes and favorite pastimes. These artistic creation must attract the visitors. It is a Holy place, generally people come here to pray and see the ancient heritage architecture.\r\n', 25.801485, 88.666738, '', '', 0),
(19, 'Jaflong', 'Gowainghat Upazila, Sylhet\r\n', 'Jaflong is a wonderful and most attractive natural visiting place of Sylhet.It attracts the tourists with its spontaneous flow of cristal clear water through the valley of hills. It is situated besides the river Mari. You can see stone collection from the river and can enjoy boating in river Mari. Jaflong is totally a hilly area of real natural beauty so don’t miss it if you like hill trackinging. You can also see wild animal in the hilly forest. You can enjoy the lifestyle of Tribe Khashia in Jaflong also.\r\n', 25.163383, 92.017524, '', '', 0),
(20, 'Kuakata', 'Kuakata, Patuakhali\r\n', 'Kuakata (Sagar Kannya / Daughter of the Sea) sea beach is a white sandy beach. It is about 18 km long and 3.5km wide. It is one of the rarest places to see the full view of both sunrise and sunset from the same place or same position. This beach is surrounded by green trees and beside the beach there are many garden forest, like The Foyej Miyar Coconut Garden, Lembur Chor, Jhau bon, Gangamotir Chor etc. These gardens and forests are the most attracting part for the tourist. There are no quick sands in the beach. So you can frequently run, take bath, swim, and pick up cockle from the beach. There are two rivers, named Payra and Bishkhali are West of the beach and river named Agunmukha is East of the beach. It has also some wanderings sites like the Rakhain Polly, Shima Buddhu Bihar, Fatrar Chor,Rash Mela, Shutki Polli that are enjoyable for the tourist.\r\n', 21.821042, 90.121423, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `places_image`
--

CREATE TABLE IF NOT EXISTS `places_image` (
  `imageId` int(5) NOT NULL,
  `locationID` int(5) NOT NULL,
  `imgURL` varchar(50) NOT NULL,
  PRIMARY KEY (`imageId`),
  KEY `locationID` (`locationID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `places_image`
--

INSERT INTO `places_image` (`imageId`, `locationID`, `imgURL`) VALUES
(101, 1, 'coxbazar1.jpg'),
(102, 1, 'coxbazar2.jpg'),
(111, 2, 'himchori1.jpg'),
(112, 2, 'himchori2.jpg'),
(121, 3, 'bandarban1.jpg'),
(122, 3, 'bandarban2.jpg'),
(131, 4, 'saintmartin1.jpg'),
(132, 4, 'saintmartin2.jpg'),
(141, 12, 'ShatGombujMasjid1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `suggested`
--

CREATE TABLE IF NOT EXISTS `suggested` (
  `name` varchar(30) NOT NULL,
  `location` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_image`
--

CREATE TABLE IF NOT EXISTS `user_image` (
  `image_ID` int(5) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(50) NOT NULL,
  `image_url` varchar(100) NOT NULL,
  `locationID` int(5) NOT NULL,
  `caption` varchar(500) NOT NULL,
  PRIMARY KEY (`image_ID`),
  KEY `user_email` (`user_email`),
  KEY `locationID` (`locationID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `password` varchar(35) NOT NULL,
  `privilige` int(3) NOT NULL,
  `resetPass` varchar(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`firstName`, `lastName`, `dob`, `gender`, `email`, `phone`, `password`, `privilige`, `resetPass`) VALUES
('Nadimul', 'Haque', '0000-00-00', 'male', 'nadim@gmail.com', '11111111111', 'bb912495fcca5b6197e0643b63a3b14b', 111, '0'),
('Md Nadimul Haque', 'Bhuiyan', '0000-00-00', 'male', 'nadim.hq321@gmail.com', '01827090222', '4a47bfcc82473724fdfd0c1d61cc85ba', 957, '0');

-- --------------------------------------------------------

--
-- Table structure for table `user_vote`
--

CREATE TABLE IF NOT EXISTS `user_vote` (
  `user_email` varchar(50) NOT NULL,
  `vote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_vote`
--

INSERT INTO `user_vote` (`user_email`, `vote`) VALUES
('nadim@gmail.com', 1),
('abcd@gmail.com', 2),
('abcd@gmail.com', 3),
('abcd@gmail.com', 4),
('nadim@gmail.com', 4),
('nadim@gmail.com', 6);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
