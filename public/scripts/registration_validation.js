
	function validate()
	{
		var error="";
		var fName = $("#first_name").val(); 
		var lName = $("#last_name").val();
		var email = $("#email").val();
		var phone = $("#phone").val();
		var pass = $("#password").val();
		var confPass = $("#conf_pass").val();


	    if (fName == "") 
	    {
	        error = "First Name must be filled out";
	        $('#span_first_name').html(error);
	        return false;
	    }
	    else
	    {
	    	error = "";
	        $('#span_first_name').html(error);
	    }
	 
	    if (lName == "") 
	    {
	        error = "Last Name must be filled out";
	        $('#span_last_name').html(error);
	        return false;
	    }
	    else
	    {
	    	error = "";
	        $('#span_last_name').html(error);
	    }


		var result = $('input[type="radio"]:checked');
		if (result.length > 0)
		{
			error = "";
	        $('#span_gender').html(error);
		}
		else
		{
			error = "Please choose gender";
	        $('#span_gender').html(error);
			return false;
		} 

        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(email != "")
        {
        	if( !emailReg.test(email))
	        {
	            error = "Please enter valid email";
		        $('#span_email').html(error);
		        return false;
	        }
	        else
	        {
	            error = "";
		        $('#span_email').html(error);
        	}
        }
        else
        {
        	error = "Enter email address";
		    $('#span_email').html(error);
		    return false;
        }
        
		
		if( phone != "")
		{
			if(phone.length != 11)
			{
				 error = "Phone number must be exactly 11 digits";
				 $("#span_phone").html(error);
				 return false;
			}
			else
			{
				error = "";
				$("#span_phone").html(error);
			}
		}
		else
	    {
	    	error = "Please provide phone number";
	        $('#span_phone').html(error);
	        return false;
	    }


		if( pass != "")
		{
			error = "";
			if(pass.length >7)
			{
				re1 = /[A-Z]/;re2 = /[a-z]/;re3 = /[0-9]/;
				if(re1.test(pass) && re2.test(pass) && re3.test(pass)) 
				{
					$('#span_password').html(error);
					if(confPass!= "")
					{
						if(pass == confPass)
						{
							return;
						}
						else
						{
							$("#span_conf_pass").html("Pass do not match to each other");
							return false;
						}
					}
					else
					{
						$("#span_conf_pass").html("Please enter confirm password field");
						return false;
					}
			    }
			    else
			    {
			    	error = "Password must contain at least one uppercase,one lowercase letter and a number";
			    }
			}
			else
			{
				error = "Password should be at least 8 charecters!";
			}
	        $('#span_password').html(error);
	        return false;		
		}
		else
	    {
	    	 error = "Please provide password";
			 $("#span_password").html(error);
			 return false;
	    }
	}
	
	
	
	
function des()
{
	if(document.getElementById("first_name").value!="" && document.getElementById("last_name").value!="" && document.getElementById("email").value!="" && document.getElementById("phone").value!="" && document.getElementById("password").value!="" && document.getElementById("conf_pass").value!="")
	{
		document.getElementById("buttonRegistration").disabled = false;
		document.getElementById("buttonRegistration").style.color= "black";
		
	}
	
	else
	{
		document.getElementById("buttonRegistration").disabled = true;
		document.getElementById("buttonRegistration").style.color= "gray";
	}
	
}
	
	
	
	
	
	
	
	
	
	function passlengthchk()
{
	
	v=document.getElementById("password").value.length;
	
	ct=0;
	nt=0;
	
	
	cap=document.getElementById("password").value;
	
	for(i=0;i<v;i++)
	{
		if((cap[i]>='A' && cap[i]<='Z') || (cap[i]>='a' && cap[i]<='z') )
		{
			ct=1;
			break;
		}
	}
	
		
	for(i=0;i<v;i++)
	{
		if(cap[i]>='1' && cap[i]<='9')
		{
			nt=1;
			break;
		}
	}
	

	
	
	if(v>=8 )
	{
		if(ct==1 &&  nt==1)
		{
			document.getElementById("passsidemsg").innerHTML="OK";
			document.getElementById("passsidemsg").style.color="green";
		}
		else
		{
			document.getElementById("passsidemsg").innerHTML="Character And Number Required";
			document.getElementById("passsidemsg").style.color="red";
		}
		
	}
	
	
	else
	{
		document.getElementById("passsidemsg").innerHTML="must be atleast 8 chararcter";
		document.getElementById("passsidemsg").style.color="red";
	}
	
	
	
}



function confrpass()
{
	
	pa=document.getElementById("password").value;
	con=document.getElementById("conf_pass").value;
	
	
	
	if(pa==con)
	{
		document.getElementById("sidemsg2").innerHTML="matched";
		document.getElementById("sidemsg2").style.color="green";
	}
	
	else
	{
		document.getElementById("sidemsg2").innerHTML="Password do not match";
		document.getElementById("sidemsg2").style.color="red";
	}
}











function loadEmail()
{
	var email = $('#email').val();

	$.ajax({
			url: 'http://localhost/TG/registration/verifyEmail',
			data: {mail: email},
			success: function(result){
				//alert(result);
			$('#span_email').html(result);
		}
	});
}